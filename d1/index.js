console.log ('Hello World!');

/* Syntax: while (expression/condition){
	statement
}

*/


//Create a function that could display a message to self
//display a message to your past self in your console 10 time
//invoke the function 10 times...


function displayMsgToSelf(){
	console.log("dont text her back");
}

displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();



let count3 = 10;

while (count3 !== 0){
	console.log("Dont text her Back")
	count3--;
}

//If there is no condition the condition is always true, thus an infinite loop

let count1 = 5;

while (count1 !== 0){
	console.log(count1)
	count1--;
}

//mini activity:
let count2 = 5;

while (count2 !== 6){
	console.log (count2)
	count2++;
}

//Do while loop

//A do while guarantee a code will run once


/*

Syntax

 do {
	statment
 } while(expression/condition)


*/


let doWhileCounter = 1;

do {
	console.log(doWhileCounter);
	doWhileCounter++;

} while(doWhileCounter <= 20)



let number = Number(prompt("Give me a number"));


do {
	console.log("Do While:" + number);
   number += 1;
}while (number < 10);

//For Loop 

/* It consist of three parts

1. initialization - value that will track the progress of the loop
2.expression/condition that will be evaluated which will determine whether
the loop will run one more time.
3.Final Expression indicates how to advance the loop(++,--)

syntax:
for (Initialization; expression/condition; finalExpression)
	*/

for(let count = 0; count <= 20; count++){
	console.log(count);
}

//For loops accessing array items

let fruits = ["Apple","Durian","Kiwi","Pineapple","Mango"];
console.log(fruits[2]);
console.log(fruits.length);
console.log(fruits[fruits.length-1]);

for(let index = 0; index < fruits.length; index++){
	console.log(fruits[index]);
}



let countries = ["belgium","canada","Philippines","Japan","russia","ukrain"];

for(let index = 0; index < countries.length; index++){
	console.log(countries[index -1]);
}

//for loops accessing elements of a string

let myString = "alex";
//.length it is also a property used in strings
console.log(myString.length); //4

//Individual characters of a string can be access using it's index number. and its also start with 0 - nth number.
console.log(myString[0])

//will create a loop that will print out the individual letters of the myString variable

for (let x = 0; x < myString.length; x++) {
	console.log(myString[x])
}



let myName = "Jane";

/*
Create a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is a vowel

*/

for(let i = 0; i < myName.length; i++) {
	//if the character of your name is a vowel letter, display number "3"
	//console.log(myName[i]);
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
		) {
		console.log(3);
	} else {
		//Print in all the non-vowel in the console 
		console.log(myName[i])
	}
}



//Continue and Break statements.

//continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block({})

//break statement is used to terminate the current loop once a match has been found



for(let count = 0; count <= 20; count++) {
	//if remainder is equal to 0, we will use the continue statement
	if(count % 2 === 0) {

		//Tells the code to continue to the next iteration
		//This ignores all statements located after the continue statement;
		continue;
	}
	//The current value of a number is printed out if the remainder is not equal to 0
	console.log("Continue and Break: " + count);


	//If the current value of count is greater than 10, then use the break
	if(count > 10) {
		break;
	}
}



//create a loop that will iterate  based on the length of the string

let name = "alexandro";

for (let i = 0; i < name.length; i++) {
	console.log(name[i])

	//if the vowel is equal to a, continue to the next iteration of the loop
	if(name[i].toLowerCase() === "a") {
		console.log("Continue to the next iteration")
		continue;
	}

	if(name[i] == "d") {
		break;
	}
}
